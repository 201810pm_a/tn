﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionAnimator : StateMachineBehaviour {

    private int _actionNum;

    //OnStateEnter is called before OnStateEnter is called on any state inside this state machine

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetInteger("ActionNum", -1);
        animator.SetInteger("MultiAction", -1);

        /*
         *if (animator.GetNextAnimatorStateInfo(layerIndex).shortNameHash
            == Animator.StringToHash("Exit"))
        {
            //animator.Play("Grounded");
            animator.SetInteger("GoToMachine",0);
            Debug.LogError("innnnnnnnnnn" );
        }

         Debug.LogError("hash  " + Animator.StringToHash("myname"));
         Debug.LogError("hash  " + Animator.StringToHash("exitname"));
         Debug.LogError("nnnnn  " + animator.GetAnimatorTransitionInfo(layerIndex).userNameHash);
         Debug.LogError("nnnnn  " + animator.GetAnimatorTransitionInfo(layerIndex).nameHash);
          Debug.LogError("nnnnn  " + animator.GetNextAnimatorStateInfo(layerIndex).IsName("Exit"));
        Debug.LogError("count Enter  " + animator.GetNextAnimatorClipInfoCount(layerIndex));
         Debug.LogError("Action OnStateEnter");
         Debug.LogError("stateInfo Enter  " + stateInfo.fullPathHash);
         Debug.LogError("stateInfo Enter  " + stateInfo.speed);
         Debug.LogError("Enter  " + animator.GetCurrentAnimatorStateInfo(layerIndex).IsName("Exit"));
         Debug.LogError("Enter  " + animator.GetCurrentAnimatorStateInfo(layerIndex).GetType().Name);
         Debug.LogError("Enter  " + animator.GetCurrentAnimatorStateInfo(layerIndex).ToString());
         _actionNum = animator.GetInteger("ActionNum");
        animator.SetInteger("ActionNum", -1);
        */
    }

    ////OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.LogError("OnStateUpdate");
        /*
         * if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            animator.SetBool("IsAction", true);
        }
        */
    }

    ////OnStateExit is called before OnStateExit is called on any state inside this state machine
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.LogError("OnStateExit");
        // animator.SetInteger("ActionNum", -1);

        /*
        Debug.LogError("mmmmmmm  " + animator.GetNextAnimatorStateInfo(layerIndex).shortNameHash);

         Debug.LogError("mmmmmmmm  " + animator.GetAnimatorTransitionInfo(layerIndex).userNameHash);
        Debug.LogError("mmmmmmm  " + animator.GetAnimatorTransitionInfo(layerIndex).nameHash);
         Debug.LogError("mmmmmmm  " + animator.GetNextAnimatorStateInfo(layerIndex).IsName("Exit"));
       Debug.LogError("count Exit  " + animator.GetNextAnimatorClipInfoCount(layerIndex));
        Debug.LogError("stateInfo Exit  " + stateInfo.fullPathHash);
        Debug.LogError("Exit stateInfo " + stateInfo);
        Debug.LogError("Exit  " + animator.GetCurrentAnimatorStateInfo(layerIndex).IsName("Exit"));
        Debug.LogError("Exit  " + animator.GetCurrentAnimatorStateInfo(layerIndex).shortNameHash.ToString());
        Debug.LogError("Exit  " + animator.GetCurrentAnimatorStateInfo(layerIndex).GetType().Name);
        Debug.LogError("Exit  " + animator.GetCurrentAnimatorStateInfo(layerIndex).ToString());
        Debug.LogError("Exit  " + animator.name);
        Debug.LogError("stateInfo Exit  " + stateInfo.speed);
        */
        //Debug.LogError("Exit  " + animator.GetAnimatorTransitionInfo(layerIndex).nameHash);

    }

    ////OnStateMove is called before OnStateMove is called on any state inside this state machine
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    Debug.LogError("OnStateMove");
    //}

    ////OnStateIK is called before OnStateIK is called on any state inside this state machine
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    Debug.LogError("OnStateIK");
    //}

    ////OnStateMachineEnter is called when entering a statemachine via its Entry Node
    //override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    //{
    //    Debug.LogError("OnStateMachineEnter");
    //}

    ////OnStateMachineExit is called when exiting a statemachine via its Exit Node
    //override public void OnStateMachineExit(Animator animator, int stateMachinePathHash)
    //{
    //    Debug.LogError("OnStateMachineExit");
    //}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityChanController : MonoBehaviour
{
    //Unityちゃんのアニメーションを格納
    Animator animator;

    // Use this for initialization
    void Start()
    {
        //UnityちゃんのAnimatorコンポーネントを取得
        this.animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {


        if (Input.GetKey(KeyCode.Alpha1))
        {
            this.animator.SetBool("Kick", true);

        }
        else
        {
            this.animator.SetBool("Kick", false);

        }
    }

    //オブジェクトに触れた時の処理
    private void OnTriggerEnter(Collider other)
    {
        /*
        //オブジェクトがGoalの場合
        if (other.gameObject.name == "Goal")
        {
            Debug.Log("33333333333333333   Input.GetKey(KeyCode.Alpha1)");
            //勝利モーションを表示
            animator.SetBool("Win", true);
        }
        else
        {
            this.animator.SetBool("Win", false);
        }

    */
    }

    public void Touch()
    {

        Destroy(this.gameObject);

    }

}
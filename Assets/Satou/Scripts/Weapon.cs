﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {
    Collider _hitPointCollider;

    public Transform handle;
    public List<GameObject> effects;

    public enum WeaponType
    {
        Sword = 1,
        Spear = 2,
        Gun = 3
    }
    public WeaponType Type;

    public Bullet bullet;

    public Collider HitPointCollider
    {
        get
        {
            return _hitPointCollider;
        }

    }



    private void Awake()
    {
        _hitPointCollider = GetComponent<Collider>();
        _hitPointCollider.enabled = false;
    }

    // Use this for initialization
    void Start () {

    }

    // Update is called once per frame
    void Update () {
		
	}


    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Weapon.OnTriggerEnter()");
        if (other.gameObject.GetComponent<Enemy>())
        {
            Debug.LogError("OnTriggerEnter innnnnnnnnn");
            Enemy enemy = other.gameObject.GetComponent<Enemy>();
            enemy.Damage(15);
        }
    }



}

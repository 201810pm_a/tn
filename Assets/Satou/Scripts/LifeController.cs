﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeController : MonoBehaviour {
    public int maxLife = 100;
    int currentLife;
    Text lifeText;

    IEnumerator CureLife(int cure)
    {
        Debug.Log("実行");
        while(currentLife < maxLife)
        {
            yield return new WaitForSeconds(1.0f);
            currentLife += cure;
            lifeText.text = currentLife.ToString();
        }

    }




    // Use this for initialization
    void Start()
    {
        //現在の体力を表示
        currentLife = 25;
        lifeText = gameObject.GetComponent<Text>();
        //５ずつ体力回復
        StartCoroutine("CureLife", 5);
    }

    // Update is called once per frame
    void Update()
    {

    }
}

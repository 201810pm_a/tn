﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : StateMachineBehaviour {
    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Debug.LogError("Attack OnStateEnter");
        animator.SetInteger("ActionNum", -1);
    }

    // OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    Debug.LogError("OnStateUpdate");
    //    if (Input.GetKeyDown(KeyCode.Alpha1))
    //    {
    //        animator.SetInteger("ActionNum", 1);
    //    }
    //}

    //// OnStateExit is called before OnStateExit is called on any state inside this state machine
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{

    //    animator.SetInteger("ActionNum", -1);
    //    Debug.LogError("OnStateExit");
    //}

    //// OnStateMove is called before OnStateMove is called on any state inside this state machine
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    Debug.LogError("OnStateMove");
    //}

    //// OnStateIK is called before OnStateIK is called on any state inside this state machine
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    Debug.LogError("OnStateIK");
    //}

    //// OnStateMachineEnter is called when entering a statemachine via its Entry Node
    //override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    //{
    //    Debug.LogError("OnStateMachineEnter");
    //}

    //// OnStateMachineExit is called when exiting a statemachine via its Exit Node
    //override public void OnStateMachineExit(Animator animator, int stateMachinePathHash)
    //{
    //    //animator.SetInteger("AttackNum", 0);
    //    Debug.LogError("OnStateMachineExit");
    //}
}

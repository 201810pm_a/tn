﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GoalController : MonoBehaviour
{
    public GameObject clearText;
    public int sceneNum = 2;//シーン番号



    //コルーチン
    IEnumerator StageClear()
    {
        //ゲームクリアテキスト表示
        this.clearText.SetActive(true);
        //3秒待つ
        yield return new WaitForSeconds(3.0f);
        //次のステージへ
        string sceneText = "Scene" + sceneNum;
        SceneManager.LoadScene(sceneText);
    }


    // Use this for initialization
    void Start()
    {
        //ゲームクリアテキストを非表示
        this.clearText.SetActive(false);
    }


    public void OnTriggerEnter(Collider other)
    {
        //ステージクリア処理
        //StartCoroutine("StageClear");

    }


    // Update is called once per frame
    void Update()
    {
    }
}
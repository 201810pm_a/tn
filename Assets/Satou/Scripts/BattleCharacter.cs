﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class BattleCharacter : MonoBehaviour
{
    public Transform swordHandle;
    public Transform spearHandle;
    public Transform gunHandle;
    private Transform _handle;
    public Weapon weaponPrefab;

    Weapon _weapon;
    Animator _animator;

    //delegate void SomeDelegate(Func<string, bool> func);
    Func<string, bool> _callback;
    int _callbackFrame;

    public Weapon Weapon
    {
        get
        {
            return _weapon;
        }

        set
        {
            _weapon = value;
        }
    }

    // Use this for initialization
    void Start()
    {
        _animator = GetComponent<Animator>();

        if (weaponPrefab != null)
        {
            WeaponAttach(weaponPrefab);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // _weapon.transform.position -= (_weapon.handle.transform.position - _handle.transform.position);
        //_weapon.transform.position = _weapon.transform.TransformPoint(_weapon.handle.transform.localPosition * -1.0f);

        // LateCallback();

    }

    internal void Damage(int v)
    {
        _animator.SetTrigger("Damage");
    }

    void LateCallback()
    {
        if (_callback != null)
        {
            if (_callbackFrame > 0)
            {
                _callback(null);
                _callback = null;
                _callbackFrame = 0;
            }
            else
            {
                _callbackFrame++;
            }

        }
    }

    public void WeaponAttach(Weapon wp)
    {
        Debug.LogError("wp : " + wp);
        if (_weapon != null)
        {
            Destroy(_weapon.gameObject);
            _weapon = null;
            Debug.LogError(_weapon);
        }

        if (_weapon == null)
        {
            Debug.LogError("nullllllllllllllll");
        }

        _handle = GetHandle((int)wp.Type);
        _weapon = (Weapon)Instantiate(
            wp,
            _handle.position,
            _handle.rotation);

        _weapon.HitPointCollider.enabled = false;

        _animator.SetInteger("WeaponType", (int)_weapon.Type);


        _weapon.transform.parent = _handle.transform;
        _weapon.handle.transform.parent = _weapon.transform;
        //_weapon.transform.localPosition = Vector3.zero;

        Debug.LogError("_weapon : " + _weapon.transform.localPosition);
        Debug.LogError("handle : " + _weapon.handle.transform.localPosition);

        // _weapon.transform.position -= (_weapon.handle.transform.position - handle.transform.position);
        _weapon.transform.localPosition = _weapon.handle.transform.localPosition * -1.0f;
        //_weapon.transform.position = _weapon.transform.TransformPoint(_weapon.handle.transform.localPosition * -1.0f);

        Debug.LogError("_weapon after : " + _weapon.handle.transform.localPosition);
        _weapon.transform.localRotation = _weapon.handle.transform.localRotation;

    }

    private Transform GetHandle(int type)
    {
        switch (type)
        {
            case 1:
                return swordHandle;
            case 2:
                return spearHandle;
            case 3:
                return gunHandle;
            default:
                return null;
        }
    }

    internal void AttackTest()
    {
        throw new NotImplementedException();
    }

    public void Action(int actionNum)
    {
        Debug.LogError("ActionNum : " + (_animator.GetInteger("ActionNum") + 1));
        _animator.SetInteger("ActionNum", actionNum);
        _animator.SetInteger("MultiAction", _animator.GetInteger("MultiAction") + 1);
    }

    private IEnumerator RunLate(Func<string, bool> callback)
    {
        throw new NotImplementedException();
    }


    void Hit()
    {
        Debug.LogError("Hitttttttttt");
    }
    void FootL()
    {
        Debug.LogError("FootL");
    }
    void FootR()
    {
        Debug.LogError("FootR");
    }

    void HitStart()
    {
        Debug.LogError("HitStart");
        _weapon.HitPointCollider.enabled = true;
    }

    void HitEnd()
    {
        Debug.LogError("HitEnd");
        _weapon.HitPointCollider.enabled = false;
    }

    void GunShootSingle(int num)
    {
        Debug.LogError("GunShootSingle");
        if ((int)_weapon.Type == 3 && _weapon.bullet != null)
        {
            Bullet bullet = Instantiate(
            _weapon.bullet,
            _weapon.transform.position,
            _weapon.transform.rotation);

            Vector3 pv = transform.forward * 100;
            //プレイヤーのrigidbodyに↑の数値分だけ力を加える。
            Rigidbody rigidbody= bullet.gameObject.GetComponent<Rigidbody>();
            rigidbody.AddForce(pv, ForceMode.VelocityChange);
            if(num == 1 && bullet.effect != null)
            {
                GameObject ef = Instantiate(
                 bullet.effect,
                 _weapon.transform.position,
                 _weapon.transform.rotation);
                ef.transform.parent = bullet.transform;

            }
        }

    }

    void Effect(int num)
    {
        Debug.LogError("num  " + num);
        GameObject effect = Instantiate(
            _weapon.effects[num],
            _weapon.transform.position,
            Quaternion.identity);

    }

}